package com.epam.rd.autotasks.chesspuzzles;

import java.util.Collection;

public interface ChessBoard {
    static ChessBoard of(Collection<ChessPiece> pieces){
        int size = 8;
        char[][] boardState = new char[size][size];

        // Initialize the board with empty cells
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                boardState[row][col] = '.';
            }
        }

        // Place the chess pieces on the board
        for (ChessPiece piece : pieces) {
            Cell cell = piece.getCell();
            int row = 8 - cell.number;
            int col = cell.letter - 'A';
            boardState[row][col] = piece.toChar();
        }

        return new ChessBoardImpl(boardState);
    }


    String state();
}

