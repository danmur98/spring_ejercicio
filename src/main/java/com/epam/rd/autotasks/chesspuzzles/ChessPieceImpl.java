package com.epam.rd.autotasks.chesspuzzles;

public class ChessPieceImpl implements ChessPiece {
    private final Cell cell;
    private final char representation;

    public ChessPieceImpl(Cell cell, char representation) {
        this.cell = cell;
        this.representation = representation;
    }

    @Override
    public Cell getCell() {
        return cell;
    }

    @Override
    public char toChar() {
        return representation;
    }
}
