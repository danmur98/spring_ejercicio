package com.epam.rd.autotasks.chesspuzzles;

public class ChessBoardImpl implements ChessBoard {
    private final char[][] boardState;

    public ChessBoardImpl(char[][] boardState) {
        this.boardState = boardState;
    }

    @Override
    public String state() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                sb.append(boardState[i][j]);
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
