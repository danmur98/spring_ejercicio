package com.epam.rd.autotasks.chesspuzzles.config;

import com.epam.rd.autotasks.chesspuzzles.Cell;
import com.epam.rd.autotasks.chesspuzzles.ChessBoard;
import com.epam.rd.autotasks.chesspuzzles.ChessPiece;
import com.epam.rd.autotasks.chesspuzzles.ChessPieceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class Default {

    @Bean
    public ChessBoard defaultChessBoard() {
        List<ChessPiece> pieces = Arrays.asList(
                new ChessPieceImpl(Cell.cell('A', 8), 'R'),
                new ChessPieceImpl(Cell.cell('B', 8), 'N'),
                new ChessPieceImpl(Cell.cell('C', 8), 'B'),
                new ChessPieceImpl(Cell.cell('D', 8), 'Q'),
                new ChessPieceImpl(Cell.cell('E', 8), 'K'),
                new ChessPieceImpl(Cell.cell('F', 8), 'B'),
                new ChessPieceImpl(Cell.cell('G', 8), 'N'),
                new ChessPieceImpl(Cell.cell('H', 8), 'R'),
                new ChessPieceImpl(Cell.cell('A', 7), 'P'),
                new ChessPieceImpl(Cell.cell('B', 7), 'P'),
                new ChessPieceImpl(Cell.cell('C', 7), 'P'),
                new ChessPieceImpl(Cell.cell('D', 7), 'P'),
                new ChessPieceImpl(Cell.cell('E', 7), 'P'),
                new ChessPieceImpl(Cell.cell('F', 7), 'P'),
                new ChessPieceImpl(Cell.cell('G', 7), 'P'),
                new ChessPieceImpl(Cell.cell('H', 7), 'P'),
                new ChessPieceImpl(Cell.cell('A', 2), 'p'),
                new ChessPieceImpl(Cell.cell('B', 2), 'p'),
                new ChessPieceImpl(Cell.cell('C', 2), 'p'),
                new ChessPieceImpl(Cell.cell('D', 2), 'p'),
                new ChessPieceImpl(Cell.cell('E', 2), 'p'),
                new ChessPieceImpl(Cell.cell('F', 2), 'p'),
                new ChessPieceImpl(Cell.cell('G', 2), 'p'),
                new ChessPieceImpl(Cell.cell('H', 2), 'p')
        );

        return ChessBoard.of(pieces);
    }
}

