package com.epam.rd.autotasks.chesspuzzles.config;

import com.epam.rd.autotasks.chesspuzzles.Cell;
import com.epam.rd.autotasks.chesspuzzles.ChessBoard;
import com.epam.rd.autotasks.chesspuzzles.ChessPiece;
import com.epam.rd.autotasks.chesspuzzles.ChessPieceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;



@Configuration
public class DefaultBlack {
    @Bean
    public ChessBoard defaultBlackChessBoard() {
        List<ChessPiece> pieces = Arrays.asList(
                new ChessPieceImpl(Cell.cell('A', 1), 'r'),
                new ChessPieceImpl(Cell.cell('B', 1), 'n'),
                new ChessPieceImpl(Cell.cell('C', 1), 'b'),
                new ChessPieceImpl(Cell.cell('D', 1), 'q'),
                new ChessPieceImpl(Cell.cell('E', 1), 'k'),
                new ChessPieceImpl(Cell.cell('F', 1), 'b'),
                new ChessPieceImpl(Cell.cell('G', 1), 'n'),
                new ChessPieceImpl(Cell.cell('H', 1), 'r'),
                new ChessPieceImpl(Cell.cell('A', 2), 'p'),
                new ChessPieceImpl(Cell.cell('B', 2), 'p'),
                new ChessPieceImpl(Cell.cell('C', 2), 'p'),
                new ChessPieceImpl(Cell.cell('D', 2), 'p'),
                new ChessPieceImpl(Cell.cell('E', 2), 'p'),
                new ChessPieceImpl(Cell.cell('F', 2), 'p'),
                new ChessPieceImpl(Cell.cell('G', 2), 'p'),
                new ChessPieceImpl(Cell.cell('H', 2), 'p')
        );

        return ChessBoard.of(pieces);
    }
}
