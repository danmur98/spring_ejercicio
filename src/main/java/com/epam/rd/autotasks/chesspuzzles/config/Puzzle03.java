package com.epam.rd.autotasks.chesspuzzles.config;

import com.epam.rd.autotasks.chesspuzzles.Cell;
import com.epam.rd.autotasks.chesspuzzles.ChessBoard;
import com.epam.rd.autotasks.chesspuzzles.ChessPiece;
import com.epam.rd.autotasks.chesspuzzles.ChessPieceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;


public class Puzzle03 {
    @Bean
    public ChessBoard puzzle03ChessBoard() {
        List<ChessPiece> pieces = Arrays.asList(
                new ChessPieceImpl(Cell.cell('H', 8), 'r'),
                new ChessPieceImpl(Cell.cell('D', 7), 'P'),
                new ChessPieceImpl(Cell.cell('C', 7), 'Q'),
                new ChessPieceImpl(Cell.cell('E', 7), 'P'),
                new ChessPieceImpl(Cell.cell('F', 7), 'r'),
                new ChessPieceImpl(Cell.cell('B', 6), 'P'),
                new ChessPieceImpl(Cell.cell('D', 6), 'P'),
                new ChessPieceImpl(Cell.cell('F', 6), 'P'),
                new ChessPieceImpl(Cell.cell('D', 5), 'p'),
                new ChessPieceImpl(Cell.cell('F', 5), 'K'),
                new ChessPieceImpl(Cell.cell('A', 4), 'B'),
                new ChessPieceImpl(Cell.cell('C', 3), 'R'),
                new ChessPieceImpl(Cell.cell('H', 2), 'p'),
                new ChessPieceImpl(Cell.cell('A', 1), 'R'),
                new ChessPieceImpl(Cell.cell('C', 1), 'b'),
                new ChessPieceImpl(Cell.cell('G', 1), 'k')
        );

        return ChessBoard.of(pieces);
    }
}
