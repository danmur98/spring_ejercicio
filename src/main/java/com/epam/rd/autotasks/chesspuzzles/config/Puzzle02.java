package com.epam.rd.autotasks.chesspuzzles.config;

import com.epam.rd.autotasks.chesspuzzles.Cell;
import com.epam.rd.autotasks.chesspuzzles.ChessBoard;
import com.epam.rd.autotasks.chesspuzzles.ChessPiece;
import com.epam.rd.autotasks.chesspuzzles.ChessPieceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class Puzzle02 {
    public ChessBoard puzzle02ChessBoard() {
    List<ChessPiece> pieces = Arrays.asList(
            new ChessPieceImpl(Cell.cell('D', 1), 'R'),
            new ChessPieceImpl(Cell.cell('B', 2), 'k'),
            new ChessPieceImpl(Cell.cell('E', 2), 'n'),
            new ChessPieceImpl(Cell.cell('F', 3), 'B'),
            new ChessPieceImpl(Cell.cell('D', 4), 'P'),
            new ChessPieceImpl(Cell.cell('E', 4), 'K'),
            new ChessPieceImpl(Cell.cell('D', 5), 'P'),
            new ChessPieceImpl(Cell.cell('A', 6), 'p'),
            new ChessPieceImpl(Cell.cell('G', 6), 'p'),
            new ChessPieceImpl(Cell.cell('E', 7), 'p'),
            new ChessPieceImpl(Cell.cell('H', 7), 'p'),
            new ChessPieceImpl(Cell.cell('D', 8), 'p'),
            new ChessPieceImpl(Cell.cell('G', 8), 'p')
    );

        return ChessBoard.of(pieces);
}

}
